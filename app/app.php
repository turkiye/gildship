<?php
require_once dirname(__DIR__).'/vendor/autoload.php';

$app = new Silex\Application();


$app['debug'] = true;


include __DIR__.'/registers.php';


include __DIR__.'/routes.php';

return $app;
