<?php namespace gildship\models;


Class adresModel extends baseModel {

  public function mahalleAl($d) {
    $sql = "select neighborhoodid as id, neighborhoodname as ad from neighborhood where areaID = ?";

    return $this->multiSelectQuery($sql,[$d]);

  }

  public function bolgeAl($d) {
    $sql = "select areaid as id, areaname as ad from area where countyID = ?";

    return $this->multiSelectQuery($sql,[$d]);

  }

  public function ilceAl($d) {
    $sql = "select countyid as id, countyname as ad from counties where CityID = ?";

    return $this->multiSelectQuery($sql,[$d]);

  }

  public function ilAl() {
    $sql = "select CityID as id, cityname as ad from cities";
    return $this->multiSelectQuery($sql);
  }


}
