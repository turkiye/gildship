<?php namespace gildship\models;

class baseModel {

  protected $_db;

  public function __construct($db) {

    $this->_db = $db;

  }

  protected function insertQuery($sql,$params) {
    $sth = $this->_db->prepare($sql);
    if ($sth->execute($params)) {
      return true;
    } else {
      return false;
    }
  }

  protected function multiSelectQuery($sql, $params=null) {
    $sth = $this->_db->prepare($sql);

    if ($sth->execute($params)) {
      return $sth->fetchAll(\PDO::FETCH_ASSOC);
    } else {
      var_dump($sth->errorInfo());
      return array();
    }
  }

}
