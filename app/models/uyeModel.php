<?php namespace gildship\models;


Class uyeModel {

  private $_db;

  public function __construct($db) {

    $this->_db = $db;

  }

  private function insertQuery($sql,$params) {
    $sth = $this->_db->prepare($sql);
    if ($sth->execute($params)) {
      return true;
    } else {
      return false;
    }
  }

  public function kaydet($uye) {
    $sql = "insert into uyeler (ad , soyad, telefon) VALUES ( :isim, :soyisim, :telefon)";
    return $this->insertQuery($sql,$uye);

  }


}
