<?php

//$app['??']
$app->register(new IPC\Silex\Provider\PDOServiceProvider(), ['pdo.options'=>[
    'dsn'        => 'mysql:host=localhost;dbname=gildship;charset=UTF8',
    'username'   => 'root',
    'password'   => '',
    'options'    => [],
    'attributes' => [],
]]);

//$app['twig']
$app->register(new Silex\Provider\TwigServiceProvider(), ['twig.path' => dirname(__DIR__).'/views',]);
