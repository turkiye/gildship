<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


$app->get('/', function () use ($app) {


  return $app['twig']->render('anasayfa.html',[]);

});

$app->get('/adres', function (Request $r) use ($app) {

  $amdl = new gildship\models\adresModel($app['pdo.connection']);
  $iller = $amdl->ilAl();

  return $app['twig']->render('adres-duzenle.html',["iller"=>$iller]);

});



$app->post('/kayit', function (Request $request) use ($app)  {

  $data['isim'] = $request->get("isim","isim yok");
  $data['soyisim'] = $request->get("soyisim","bu da yok");
  $data['telefon'] = $request->get("telefon","18880");
  $uymdl = new gildship\models\uyeModel($app['pdo.connection']);
  $rslt = $uymdl->kaydet($data);
    return ($rslt?"Kaydedildi":"hata");
});

$app->get('/ajax/ilce-al', function (Request $req) use ($app) {
  $ilid = (int) $req->get('id',0);
  $ilceler = array();
  if ($ilid > 0) {
    $amdl = new gildship\models\adresModel($app['pdo.connection']);
    $ilceler = $amdl->ilceAl($ilid);
  }
  return json_encode($ilceler);
});

$app->get('/ajax/bolge-al', function (Request $req) use ($app) {
  $ilid = (int) $req->get('id',0);
  $ilceler = array();
  if ($ilid > 0) {
    $amdl = new gildship\models\adresModel($app['pdo.connection']);
    $ilceler = $amdl->bolgeAl($ilid);
  }
  return json_encode($ilceler);
});

$app->get('/ajax/mahalle-al', function (Request $req) use ($app) {
  $ilid = (int) $req->get('id',0);
  $ilceler = array();
  if ($ilid > 0) {
    $amdl = new gildship\models\adresModel($app['pdo.connection']);
    $ilceler = $amdl->mahalleAl($ilid);
  }
  return json_encode($ilceler);
});
